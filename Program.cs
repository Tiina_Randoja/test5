﻿using System;

namespace test5
{
    class Program
    {
        static void Main(string[] args)
        {//Start the program with Clear();
        Console.Clear();
        
        
        //End the program with blank line and instructions
        Console.ResetColor();
        Console.WriteLine("Have a great day !");
        Console.WriteLine("Press <Enter> to quit the program");
        Console.ReadKey();
           
        }
    }
}
